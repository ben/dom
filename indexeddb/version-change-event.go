// +build js,wasm

package indexeddb

import "syscall/js"

type VersionChangeEventHandler func(VersionChangeEvent)

func (f VersionChangeEventHandler) Call(v js.Value) {
	f(VersionChangeEvent{v})
}

type VersionChangeEvent struct {
	v js.Value
}

func (e VersionChangeEvent) OldVersion() uint64 {
	return uint64(e.v.Get("oldVersion").Float())
}

func (e VersionChangeEvent) NewVersion() uint64 {
	if nv := e.v.Get("newVersion"); nv != js.Null() {
		return uint64(nv.Float())
	}

	return 0
}

func (e VersionChangeEvent) JSValue() js.Value { return e.v }
