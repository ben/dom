// +build js,wasm

package indexeddb

type TransactionMode string

const (
	ReadOnly      TransactionMode = "readonly"
	ReadWrite     TransactionMode = "readwrite"
	VersionChange TransactionMode = "versionchange"
)
