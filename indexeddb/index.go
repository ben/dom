// +build js,wasm

package indexeddb

type Index struct {
	objectStoreShared
}

func (i Index) ObjectStore() ObjectStore {
	return ObjectStore{objectStoreShared{i.v.Get("objectStore")}}
}

func (i Index) MultiEntry() bool {
	return i.v.Get("multiEntry").Bool()
}

func (i Index) Unique() bool {
	return i.v.Get("unique").Bool()
}
