// +build js,wasm

package indexeddb

import (
	"syscall/js"

	"git.lubar.me/ben/dom/jsutil"
)

var factory = js.Global().Get("indexedDB")

func Open(name string) OpenDBRequest {
	return OpenDBRequest{Request{factory.Call("open", name)}}
}

func OpenVersion(name string, version uint64) OpenDBRequest {
	return OpenDBRequest{Request{factory.Call("open", name, version)}}
}

func DeleteDatabase(name string) OpenDBRequest {
	return OpenDBRequest{Request{factory.Call("deleteDatabase", name)}}
}

func Databases() (info []DatabaseInfo, err error) {
	defer jsutil.Catch(&err)
	jsutil.FromJS(&info, jsutil.Await(factory.Call("databases")))
	return
}

func Cmp(first, second Any) int {
	return factory.Call("cmp", jsutil.ToJS(first), jsutil.ToJS(second)).Int()
}
