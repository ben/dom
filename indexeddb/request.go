// +build js,wasm

package indexeddb

import (
	"context"
	"syscall/js"

	"git.lubar.me/ben/dom/jsutil"
)

type Request struct {
	v js.Value
}

func (r Request) Result(ctx context.Context) (js.Value, error) {
	if r.v.Get("readyState").String() == "done" {
		return r.v.Get("result"), jsutil.ToErr(r.v.Get("error"))
	}

	done := make(chan struct{}, 2)
	onComplete := jsutil.SingleFuncOf(func(js.Value) { done <- struct{}{} })
	defer onComplete.Release()
	r.v.Call("addEventListener", "success", onComplete)
	defer r.v.Call("removeEventListener", "success", onComplete)
	r.v.Call("addEventListener", "error", onComplete)
	defer r.v.Call("removeEventListener", "error", onComplete)

	select {
	case <-done:
		return r.v.Get("result"), jsutil.ToErr(r.v.Get("error"))
	case <-ctx.Done():
		return js.Undefined(), ctx.Err()
	}
}

func (r Request) Source() js.Value {
	return r.v.Get("source")
}

func (r Request) Transaction() Transaction {
	return Transaction{r.v.Get("transaction")}
}

func (r Request) JSValue() js.Value { return r.v }
