// +build js,wasm

package indexeddb

import (
	"syscall/js"

	"git.lubar.me/ben/dom/jsutil"
)

type objectStoreShared struct {
	v js.Value
}

func (s objectStoreShared) Name() string {
	return s.v.Get("name").String()
}

func (s objectStoreShared) SetName(name string) (err error) {
	defer jsutil.Catch(&err)
	s.v.Set("name", name)
	return
}

func (s objectStoreShared) KeyPath() KeyPath {
	return jsutil.FromJSNullStringOrArray(s.v.Get("keyPath"))
}

func (s objectStoreShared) Get(query Query) Request {
	return Request{s.v.Call("get", jsutil.ToJS(query))}
}

func (s objectStoreShared) GetKey(query Query) Request {
	return Request{s.v.Call("getKey", jsutil.ToJS(query))}
}

func (s objectStoreShared) GetAll(query Query, count uint64) Request {
	if count == 0 {
		return Request{s.v.Call("getAll", jsutil.ToJS(query))}
	}
	return Request{s.v.Call("getAll", jsutil.ToJS(query), count)}
}

func (s objectStoreShared) GetAllKeys(query Query, count uint64) Request {
	if count == 0 {
		return Request{s.v.Call("getAllKeys", jsutil.ToJS(query))}
	}
	return Request{s.v.Call("getAllKeys", jsutil.ToJS(query), count)}
}

func (s objectStoreShared) Count(query Query) Request {
	return Request{s.v.Call("count", jsutil.ToJS(query))}
}

func (s objectStoreShared) OpenCursor(query Query, direction CursorDirection) Request {
	return Request{s.v.Call("openCursor", jsutil.ToJS(query), string(direction))}
}

func (s objectStoreShared) OpenKeyCursor(query Query, direction CursorDirection) Request {
	return Request{s.v.Call("openKeyCursor", jsutil.ToJS(query), string(direction))}
}

func (s objectStoreShared) JSValue() js.Value { return s.v }
