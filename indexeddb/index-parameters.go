// +build js,wasm

package indexeddb

type IndexParameters struct {
	Unique     bool `json:"unique"`
	MultiEntry bool `json:"multiEntry"`
}
