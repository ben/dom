// +build js,wasm

package indexeddb

import (
	"syscall/js"

	"git.lubar.me/ben/dom/jsutil"
)

var keyRange = js.Global().Get("IDBKeyRange")

type KeyRange struct {
	v js.Value
}

func Only(value Any) KeyRange {
	return KeyRange{keyRange.Call("only", jsutil.ToJS(value))}
}

func LowerBound(lower Any, open bool) KeyRange {
	return KeyRange{keyRange.Call("lowerBound", jsutil.ToJS(lower), open)}
}

func UpperBound(upper Any, open bool) KeyRange {
	return KeyRange{keyRange.Call("upperBound", jsutil.ToJS(upper), open)}
}

func Bound(lower, upper Any, lowerOpen, upperOpen bool) KeyRange {
	return KeyRange{keyRange.Call("bound", jsutil.ToJS(lower), jsutil.ToJS(upper), lowerOpen, upperOpen)}
}

func (r KeyRange) Includes(key Any) bool {
	return r.v.Call("includes", jsutil.ToJS(key)).Bool()
}

func (r KeyRange) JSValue() js.Value { return r.v }
