// +build js,wasm

package indexeddb

import (
	"syscall/js"

	"git.lubar.me/ben/dom/jsutil"
)

type Cursor struct {
	v js.Value
}

func (c Cursor) Source() js.Value {
	return c.v.Get("source")
}

func (c Cursor) Direction() CursorDirection {
	return CursorDirection(c.v.Get("direction").String())
}

func (c Cursor) Key() js.Value {
	return c.v.Get("key")
}

func (c Cursor) PrimaryKey() js.Value {
	return c.v.Get("primaryKey")
}

func (c Cursor) Request() Request {
	return Request{c.v.Get("request")}
}

func (c Cursor) Advance(count uint64) {
	c.v.Call("advance", count)
}

func (c Cursor) Continue(key Any) {
	if key == nil {
		c.v.Call("continue")
	} else {
		c.v.Call("continue", jsutil.ToJS(key))
	}
}

func (c Cursor) ContinuePrimaryKey(key, primaryKey Any) {
	c.v.Call("continuePrimaryKey", jsutil.ToJS(key), jsutil.ToJS(primaryKey))
}

func (c Cursor) Update(value Any) Request {
	return Request{c.v.Call("update", jsutil.ToJS(value))}
}

func (c Cursor) Delete() Request {
	return Request{c.v.Call("delete")}
}

func (c Cursor) JSValue() js.Value { return c.v }
