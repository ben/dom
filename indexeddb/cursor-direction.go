// +build js,wasm

package indexeddb

type CursorDirection string

const (
	DirNext       CursorDirection = "next"
	DirNextUnique CursorDirection = "nextunique"
	DirPrev       CursorDirection = "prev"
	DirPrevUnique CursorDirection = "prevunique"
)
