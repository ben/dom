// +build js,wasm

package indexeddb

import (
	"context"
	"syscall/js"

	"git.lubar.me/ben/dom"
	"git.lubar.me/ben/dom/jsutil"
)

type Database struct {
	v js.Value
}

func (d Database) Name() string {
	return d.v.Get("name").String()
}

func (d Database) Version() uint64 {
	var v uint64
	jsutil.FromJS(&v, d.v.Get("version"))
	return v
}

func (d Database) ObjectStoreNames() []string {
	var s []string
	jsutil.FromJS(&s, d.v.Get("objectStoreNames"))
	return s
}

func (d Database) Transaction(storeNames []string, mode TransactionMode) (t Transaction, err error) {
	defer jsutil.Catch(&err)
	return Transaction{d.v.Call("transaction", jsutil.ToJS(storeNames), string(mode))}, nil
}

func (d Database) Close() error {
	d.v.Call("close")
	// close() does not cause a close event. clean up our listeners.
	d.v.Call("dispatchEvent", js.Global().Get("Event").New("_go_closed"))
	return nil
}

func (d Database) CreateObjectStore(name string, options ObjectStoreParameters) (s ObjectStore, err error) {
	defer jsutil.Catch(&err)
	return ObjectStore{objectStoreShared{d.v.Call("createObjectStore", name, jsutil.ToJS(options))}}, nil
}

func (d Database) DeleteObjectStore(name string) (err error) {
	defer jsutil.Catch(&err)
	d.v.Call("deleteObjectStore", name)
	return
}

func (d Database) OnAbort(f func()) context.CancelFunc {
	return jsutil.AddEventListener(d.v, "abort", func(js.Value) { f() }, "close", "_go_closed")
}

func (d Database) OnClose(f func()) context.CancelFunc {
	var cancel context.CancelFunc
	cancel = jsutil.AddEventListener(d.v, "close", func(js.Value) { f(); cancel() }, "_go_closed")
	return cancel
}

func (d Database) OnError(f dom.CancellableEventHandler) context.CancelFunc {
	return jsutil.AddEventListener(d.v, "error", f.Call, "close", "_go_closed")
}

func (d Database) OnVersionChange(f VersionChangeEventHandler) context.CancelFunc {
	return jsutil.AddEventListener(d.v, "versionchange", f.Call, "close", "_go_closed")
}

func (d Database) JSValue() js.Value { return d.v }
