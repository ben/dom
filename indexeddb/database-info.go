// +build js,wasm

package indexeddb

type DatabaseInfo struct {
	Name    string `json:"name"`
	Version uint64 `json:"version"`
}
