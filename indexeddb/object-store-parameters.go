// +build js,wasm

package indexeddb

type ObjectStoreParameters struct {
	KeyPath       KeyPath `json:"keyPath,omitempty"`
	AutoIncrement bool    `json:"autoIncrement"`
}
