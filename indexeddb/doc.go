// +build js,wasm

package indexeddb

// Any is a documentation-only type that represents JSON-serializable data.
type Any interface{}

// Query is a documentation-only type that is either Any or KeyRange.
type Query interface{}

// KeyPath is a documentation-only type that is string or []string.
type KeyPath interface{}
