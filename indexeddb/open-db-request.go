// +build js,wasm

package indexeddb

import (
	"context"

	"git.lubar.me/ben/dom/jsutil"
)

type OpenDBRequest struct {
	Request
}

func (r OpenDBRequest) Result(ctx context.Context) (Database, error) {
	db, err := r.Request.Result(ctx)
	return Database{db}, err
}

func (r OpenDBRequest) OnBlocked(f VersionChangeEventHandler) context.CancelFunc {
	return jsutil.AddEventListener(r.v, "blocked", f.Call, "success", "error")
}

func (r OpenDBRequest) OnUpgradeNeeded(f VersionChangeEventHandler) context.CancelFunc {
	return jsutil.AddEventListener(r.v, "upgradeneeded", f.Call, "success", "error")
}
