// +build js,wasm

package indexeddb

import (
	"context"
	"syscall/js"

	"git.lubar.me/ben/dom"
	"git.lubar.me/ben/dom/jsutil"
)

type Transaction struct {
	v js.Value
}

func (t Transaction) ObjectStoreNames() []string {
	var s []string
	jsutil.FromJS(&s, t.v.Get("objectStoreNames"))
	return s
}

func (t Transaction) Mode() TransactionMode {
	return TransactionMode(t.v.Get("mode").String())
}

func (t Transaction) DB() Database {
	return Database{t.v.Get("db")}
}

func (t Transaction) Err() error {
	return jsutil.ToErr(t.v.Get("error"))
}

func (t Transaction) ObjectStore(name string) ObjectStore {
	return ObjectStore{objectStoreShared{t.v.Call("objectStore", name)}}
}

func (t Transaction) Commit() (err error) {
	defer jsutil.Catch(&err)
	t.v.Call("commit")
	return
}

func (t Transaction) Abort() (err error) {
	defer jsutil.Catch(&err)
	t.v.Call("abort")
	return
}

func (t Transaction) OnAbort(f func()) context.CancelFunc {
	var cancel context.CancelFunc
	cancel = jsutil.AddEventListener(t.v, "abort", func(js.Value) {
		f()
		cancel()
	}, "complete")
	return cancel
}

func (t Transaction) OnComplete(f func()) context.CancelFunc {
	var cancel context.CancelFunc
	cancel = jsutil.AddEventListener(t.v, "complete", func(js.Value) {
		f()
		cancel()
	}, "abort")
	return cancel
}

func (t Transaction) OnError(f dom.CancellableEventHandler) context.CancelFunc {
	return jsutil.AddEventListener(t.v, "error", f.Call, "complete", "abort")
}

func (t Transaction) JSValue() js.Value { return t.v }
