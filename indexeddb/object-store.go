// +build js,wasm

package indexeddb

import "git.lubar.me/ben/dom/jsutil"

type ObjectStore struct {
	objectStoreShared
}

func (s ObjectStore) IndexNames() []string {
	var names []string
	jsutil.FromJS(&names, s.v.Get("indexNames"))
	return names
}

func (s ObjectStore) Transaction() Transaction {
	return Transaction{s.v.Get("transaction")}
}

func (s ObjectStore) AutoIncrement() bool {
	return s.v.Get("autoIncrement").Bool()
}

func (s ObjectStore) Put(value Any) Request {
	return Request{s.v.Call("put", jsutil.ToJS(value))}
}

func (s ObjectStore) PutKey(value, key Any) Request {
	return Request{s.v.Call("put", jsutil.ToJS(value), jsutil.ToJS(key))}
}

func (s ObjectStore) Add(value Any) Request {
	return Request{s.v.Call("add", jsutil.ToJS(value))}
}

func (s ObjectStore) AddKey(value, key Any) Request {
	return Request{s.v.Call("add", jsutil.ToJS(value), jsutil.ToJS(key))}
}

func (s ObjectStore) Delete(query Query) Request {
	return Request{s.v.Call("delete", jsutil.ToJS(query))}
}

func (s ObjectStore) Clear() Request {
	return Request{s.v.Call("clear")}
}

func (s ObjectStore) Index(name string) (i Index, err error) {
	defer jsutil.Catch(&err)
	return Index{objectStoreShared{s.v.Call("index", name)}}, nil
}

func (s ObjectStore) CreateIndex(name string, keyPath KeyPath, options IndexParameters) (i Index, err error) {
	defer jsutil.Catch(&err)
	return Index{objectStoreShared{s.v.Call("createIndex", name, jsutil.ToJS(keyPath), jsutil.ToJS(options))}}, nil
}

func (s ObjectStore) DeleteIndex(name string) (err error) {
	defer jsutil.Catch(&err)
	s.v.Call("deleteIndex", name)
	return
}
