// +build js,wasm

package dom

import (
	"context"
	"syscall/js"
)

type CancellableEventHandler func(context.CancelFunc)

func (f CancellableEventHandler) Call(v js.Value) {
	f(func() { v.Call("preventDefault") })
}
