// +build js,wasm

// Package jsutil contains wrappers for syscall/js functionality that make
// writing Go code simpler.
package jsutil

import (
	"encoding/json"
	"syscall/js"
)

// SingleFuncOf is a simplified version of syscall/js.FuncOf for functions with
// only one argument.
func SingleFuncOf(f func(js.Value)) js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		var v js.Value
		if len(args) >= 1 {
			v = args[0]
		}

		f(v)

		return js.Undefined()
	})
}

// ToErr wraps a JavaScript value in js.Error if it is truthy;
// otherwise, it returns nil.
func ToErr(v js.Value) error {
	if v.Truthy() {
		return js.Error{v}
	}

	return nil
}

// Await returns the result of a JavaScript promise, or throws the error if
// one occurred.
//
// Use with Catch to collect the error.
func Await(promise js.Value) js.Value {
	ch := make(chan js.Value, 1)
	errCh := make(chan js.Error, 1)

	onSuccess := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		ch <- args[0]

		return nil
	})
	defer onSuccess.Release()
	onFailure := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		errCh <- js.Error{args[0]}

		return nil
	})
	defer onFailure.Release()

	promise.Call("then", onSuccess, onFailure)

	select {
	case v := <-ch:
		return v
	case err := <-errCh:
		panic(err)
	}
}

// Catch collects a thrown JavaScript error during the exit of a Go function.
// If the error value referenced by the pointer is non-nil, it is not changed.
//
// Usage:
//
//     func Something() (err error) {
//         defer jsutil.Catch(&err)
//         // ...
func Catch(err *error) {
	if r := recover(); r != nil {
		e := r.(js.Error)

		if *err == nil {
			*err = e
		}
	}
}

var jsJSON = js.Global().Get("JSON")

// ToJS transforms a Go object to a JavaScript object via the encoding/json
// package. This is more advanced than what is possible via syscall/js.ValueOf.
func ToJS(x interface{}) js.Value {
	if w, ok := x.(js.Wrapper); ok {
		return w.JSValue()
	}

	b, err := json.Marshal(x)
	if err != nil {
		panic(err)
	}

	return jsJSON.Call("parse", string(b))
}

// FromJS transforms a JavaScript object into a Go object via the encoding/json
// package. It returns the Go object, so a type assertion can be added to use
// this function inline.
func FromJS(x interface{}, v js.Value) interface{} {
	s := jsJSON.Call("stringify", v).String()

	if err := json.Unmarshal([]byte(s), x); err != nil {
		panic(err)
	}

	return x
}

// FromJSNullStringOrArray is a specialized version of FromJS for APIs that can
// return a string, an array of strings, or a falsey value.
func FromJSNullStringOrArray(v js.Value) interface{} {
	if v.Type() == js.TypeString {
		return v.String()
	}
	if !v.Truthy() {
		return nil
	}

	var s []string
	FromJS(&s, v)
	return s
}
