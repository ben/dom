package jsutil

import (
	"context"
	"syscall/js"
)

// AddEventListener adds f as an event listener to the given event on v.
// The event listener will be removed and cleaned up if the returned function
// is called or if any of the autoCancel events are dispatched.
func AddEventListener(v js.Value, event string, f func(js.Value), autoCancel ...string) context.CancelFunc {
	var cancelled bool
	var cb, done js.Func
	cancel := func() {
		if cancelled {
			return
		}
		cancelled = true

		v.Call("removeEventListener", event, cb)
		for _, name := range autoCancel {
			v.Call("removeEventListener", name, done)
		}

		cb.Release()
		done.Release()
	}

	cb = SingleFuncOf(f)
	done = SingleFuncOf(func(js.Value) { cancel() })

	v.Call("addEventListener", event, cb)
	for _, name := range autoCancel {
		v.Call("addEventListener", name, done)
	}

	return cancel
}
